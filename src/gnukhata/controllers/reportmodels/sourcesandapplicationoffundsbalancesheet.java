/**
 * 
 */
package gnukhata.controllers.reportmodels;

/**
 * @author inu
 *
 */
public class sourcesandapplicationoffundsbalancesheet {
	private String groupName;
	private String amount1;
	private String amount2;
	/**
	 * @param groupName
	 * @param amount1
	 * @param amount2
	 */
	public sourcesandapplicationoffundsbalancesheet(String groupName,
			String amount1, String amount2) {
		super();
		this.groupName = groupName;
		this.amount1 = amount1;
		this.amount2 = amount2;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @return the amount1
	 */
	public String getAmount1() {
		return amount1;
	}
	/**
	 * @return the amount2
	 */
	public String getAmount2() {
		return amount2;
	}
	
	
}
